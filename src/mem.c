#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static struct region alloc_region  ( void const * addr, size_t query ) {
    bool extends = true;
    size_t size = region_actual_size(query + offsetof( struct block_header, contents ));
    void* new_addr = map_pages(addr, size, MAP_FIXED);
    if(new_addr == MAP_FAILED) {
        new_addr = map_pages(addr, size, 0);
        if(new_addr == MAP_FAILED) return (struct region) {0};
        extends = false;
    }

    block_init(new_addr, (block_size) {size}, NULL);

    return (struct region) {new_addr, size, extends};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(!block_splittable(block, query)) return false;

  bool block_is_free = block->is_free;

  block_capacity new_block1_capacity = {query < BLOCK_MIN_CAPACITY ? BLOCK_MIN_CAPACITY : query};
  struct block_header* new_block2_addr = (struct block_header*)(block->contents + new_block1_capacity.bytes);

  block_init(new_block2_addr, (block_size){block->capacity.bytes - new_block1_capacity.bytes}, block->next);
  new_block2_addr->is_free = block_is_free;

  block_init(block, size_from_capacity(new_block1_capacity), new_block2_addr);
  block->is_free = block_is_free;

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}


static bool _try_merge_with_next(struct block_header* block, bool is_free_insensitive) {
    if(!block->next) return false;

    if(is_free_insensitive) {
        if(!mergeable(block, block->next)) return false;
    } else if (!blocks_continuous(block, block->next)) return false;

    block_init(
            block,
            (block_size){size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes},
            block->next->next);

    return true;
}

static bool try_merge_with_next( struct block_header* block ){
    return _try_merge_with_next(block, true);
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static bool block_is_good( struct block_header* restrict block, size_t sz ) { return block && block->is_free && block->capacity.bytes >= sz; }


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if(!block) return (struct block_search_result) {BSR_CORRUPTED};

  struct block_header* prev_block = NULL;
  while(block) {

    if(try_merge_with_next(block)) continue;
    if(block_is_good(block, sz)) return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};

    prev_block = block;
    block = block->next;
  }
  return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, prev_block};
}

static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result search_result = find_good_or_last(block, query);

    if(search_result.type != BSR_FOUND_GOOD_BLOCK) return search_result;

    split_if_too_big(search_result.block, query);
    search_result.block->is_free = false;
    return search_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if(!last) return NULL;

    struct region new_region = alloc_region(block_after(last), query);
    if(region_is_invalid(&new_region)) return NULL;

    last->next = new_region.addr;
    return try_merge_with_next(last) ? last : last->next;
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    if(!heap_start) return NULL;
    query = query > BLOCK_MIN_CAPACITY ? query : BLOCK_MIN_CAPACITY;

    struct block_search_result search_result = try_memalloc_existing(query, heap_start);

    switch(search_result.type) {
        case BSR_FOUND_GOOD_BLOCK: return search_result.block;
        case BSR_CORRUPTED: return NULL;
        case BSR_REACHED_END_NOT_FOUND: {
            struct block_header *new_block = grow_heap(search_result.block, query);
            search_result = try_memalloc_existing(query, new_block);
        }
    }

    if(search_result.type == BSR_FOUND_GOOD_BLOCK) return search_result.block;
    return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

void heap_term( ) {
    struct block_header* addr = HEAP_START;
    struct block_header* next = NULL;
    while(addr) {
        while(_try_merge_with_next(addr, false)) {};
        next = addr->next;
        munmap(addr, size_from_capacity(addr->capacity).bytes);
        addr = next;
    }
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  try_merge_with_next(header);
}
